package main

import (
	"encoding/json"
	"fmt"
)

func Unmarshal[T any](data []byte, p *T) error {
	return json.Unmarshal(data, p)
}

func main() {
	data := []byte(`[1, 2, 3]`)
	var v []int
	// fmt.Println("err:", Unmarshal(data, v)) // won't compile
	fmt.Println("err:", Unmarshal(data, &v))
	fmt.Println("v:", v)
}
