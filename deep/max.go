package deep

import (
	"errors"
)

var ErrEmpty = errors.New("empty slice")

// Excercise: Make generic
func Mode[T comparable](values []T) (T, error) {
	if len(values) == 0 {
		return zero[T](), ErrEmpty
	}

	freq := make(map[T]int)
	for _, v := range values {
		freq[v]++
	}

	return MaxMap(freq)
}

func MaxMap[K comparable, V Ordered](m map[K]V) (K, error) {
	first := true
	var maxK K
	var maxV V
	for k, v := range m {
		if first {
			first = false
			maxK, maxV = k, v
			continue
		}
		if v > maxV {
			maxK, maxV = k, v
		}
	}

	return maxK, nil
}

// type Flag int // Flag is a new type
// type Flag = int // Flag is a diffent name for int (type alias)
// type byte = uint8
// type rune = int32
type Ordered interface {
	~int | ~float64 | string
}

func zero[T any]() T {
	var v T
	return v
}

// Exercise: Support both int and float64
func Max[T Ordered](values []T) (T, error) {
	if len(values) == 0 {
		// var zero T
		// return zero, ErrEmpty
		return zero[T](), ErrEmpty
	}

	m := values[0]
	for _, v := range values[1:] {
		if v > m {
			m = v
		}
	}

	return m, nil
}
