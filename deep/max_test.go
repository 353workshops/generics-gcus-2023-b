package deep

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestMax(t *testing.T) {
	v, err := Max([]int{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3, v)

	fv, err := Max([]float64{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3.0, fv)

	mv, err := Max([]time.Month{2, 3, 1})
	require.NoError(t, err)
	require.Equal(t, 3, mv)

	_, err = Max[int](nil)
	require.Error(t, err)
}
