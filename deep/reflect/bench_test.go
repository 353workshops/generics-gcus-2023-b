package deep

import "testing"

var v any

func BenchmarkRelu(b *testing.B) {
	for i := 0; i < b.N; i++ {
		v = Relu(i)
		if i > 0 && v == 0 {
			b.Fatal(i, v)
		}
	}
}
