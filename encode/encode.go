package main

import (
	"encoding/json"
	"io"
	"os"
)

func Encode[T any](w io.Writer, v T) error {
	return json.NewEncoder(w).Encode(v)
}

type User struct {
	Login string
	ID    int
}

type Admin struct {
	ID    int
	Login string
}

// GCShape stenciling
// Two types will be in the same GC stentciling group
// iff the unerlyint concrete type is the same
// All pointers belong to the same group
func main() {
	w := os.Stdout
	i := 42
	Encode(w, i)

	f := 3.14
	Encode(w, f)

	Encode(w, &i)
	Encode(w, &f)

	u := User{Login: "elliot", ID: 3}
	Encode(w, u)
	Encode(w, &u)

	a := Admin{Login: "root", ID: 1}
	Encode(w, a)
}
