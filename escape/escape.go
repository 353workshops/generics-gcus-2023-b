package main

import "fmt"

func main() {
	name, height := "Shaq", 7.1
	fmt.Printf("%s: %.2f\n", name, height)
}

// go build -gcflags=-m
