package main

import (
	"fmt"
	"time"
)

func uuid() string {
	return "451f7ed"
}

func future[T any](fn func() T) <-chan T {
	ch := make(chan T, 1)
	go func() {
		time.Sleep(time.Second)
		ch <- fn()
	}()
	return ch
}

func main() {
	ch := future(uuid)
	fmt.Println(<-ch)
}
