package main

import (
	"fmt"
)

type Access struct {
	User string
	URI  string
}

type Login struct {
	User string
}

type Event interface {
	Login | Access
}

func Handle[T Event](e T) error {
	// fmt.Printf("event from %s", e.User) // won't compile
	switch t := any(e).(type) {
	case Login:
		fmt.Printf("login by %s\n", t.User)
	case Access:
		fmt.Printf("access by %s to %s\n", t.User, t.URI)
	default:
		return fmt.Errorf("unknown event: %T", e)
	}
	return nil
}

func main() {
	Handle(Login{"elliot"})
	Handle(Access{"elliot", "/etc/passwd"})
}
