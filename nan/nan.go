package main

import (
	"fmt"
	"math"
)

func main() {
	m := map[float64]int{
		math.NaN(): 1,
	}
	delete(m, math.NaN())
	fmt.Println(m)
}
