package main

import "fmt"

type Player struct {
	Name    string  `json:"name,omitempty"`
	TagLing *string `json:"tag_ling,omitempty"`
}

func Ptr[T any](val T) *T {
	return &val
}

func main() {
	// tl := "Suit Up!"
	p := Player{
		Name:    "Barney",
		TagLing: Ptr("Suit Up!"),
	}
	fmt.Println(p)
}
