package scanner

import (
	"encoding/json"
	"errors"
	"io"
)

type Event interface {
	Login | Access
	Validate() error
	//  Validator
}

// Exercise: Support also Access in Scanner
type Scanner[T Event] struct {
	dec *json.Decoder
	err error
}

func New[T Event](r io.Reader) *Scanner[T] {
	s := Scanner[T]{
		dec: json.NewDecoder(r),
	}
	return &s
}

func (s *Scanner[T]) Err() error {
	return s.err
}

/*
type Reader interface {
	Read([]byte) (int, error)
}

type Reader interface {
	Read(int) ([]byte, error)
}
*/
// func (s *Scanner) Next() (l *Login, bool) {
// func (s *Scanner) Next(l Validator) bool {
func (s *Scanner[T]) Next(l *T) bool {
	if s.err != nil {
		return false
	}

	err := s.dec.Decode(l)
	if errors.Is(err, io.EOF) {
		return false
	}

	if err != nil {
		s.err = err
		return false
	}

	if err := (*l).Validate(); err != nil {
		s.err = err
		return false
	}

	/*
		v, ok := any(l).(Validator)
		if !ok {
			// TODO: What?
		}
		if ok {
			if err := v.Validate(); err != nil {
				s.err = err
				return false
			}
		}
	*/

	return true
}

type Validator interface {
	Validate() error
}
