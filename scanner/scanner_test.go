package scanner

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestScanner(t *testing.T) {
	file, err := os.Open("testdata/logins.jsonl")
	require.NoError(t, err, "open")
	defer file.Close()

	s := New[Login](file)
	var logs []Login
	var l Login
	for s.Next(&l) {
		logs = append(logs, l)
	}
	t.Logf("users: %#v\n", logs)
	require.NoError(t, s.Err(), "scan")
	require.Equal(t, 3, len(logs))
	for _, u := range logs {
		require.NoError(t, u.Validate())
	}
}
